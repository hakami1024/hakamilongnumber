﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace HakamiLong
{
   
    public class Calculator
    {
        private const String INCORRECT_INPUT = "the line you writed is incorrect";

        public static Long Calculate(String line){

            if( line.Length == 0 )
                throw new InvalidLongException( INCORRECT_INPUT );

            String s = "(" + line + ")";
            Long answer = (Long)changeBracesToResults(s);
            return answer;
        }

        private static String changeBracesToResults(String line)
        {
            Stack<int> beginnings = new Stack<int>();

            for( int i=0; i<line.Length; i++ )
            {
                switch (line[i])
                {
                    case '(':
                        beginnings.Push(i);
                        break;
                    case ')':
                        int openBraceIndex = beginnings.Pop();
                        String s = line.Substring(openBraceIndex + 1, i - openBraceIndex - 1);
                        String calced = calculateWithoutBraces(s);
                        line = line.Substring(0, openBraceIndex) + calced + line.Substring(i+1, line.Length-i-1);
                        i = openBraceIndex + calced.Length - 1 ;
                        break;
                }
            }

            return line;
        }

        private delegate Long Operation(Long x, Long y);

        private static readonly Dictionary<String, Operation> Operations = new Dictionary<String, Operation>
        {
                { "+", (x, y) => x + y },
                { "-", (x, y) => x - y },
                { "*", (x, y) => x*y },
                { "/", (x, y) => x/y },
                { "sqrt", (x, y) => x.Sqrt() },
                { "log2", (x, y) => x.Log2() }
            };

        private static String singleCalculation(String op, Long x, Long y)
        {
            return Operations[op](x, y).ToString();
        }

#if DEBUG
        public
#else 
        private 
#endif
            static String calculateWithoutBraces( String line ){
            Regex regex = new Regex( @"(((?<!\d)-)?[0-9]+)|\+|\-|\*|\/|sqrt|log2" );
            List< String > components = regex.Matches( line ).Cast< Match >().Select( m => m.Value ).ToList();

            for (int i = 0; i < components.Count; )
            {
                if (components[i] == "sqrt" || components[i] == "log2")
                {
                    components[ i + 1 ] = singleCalculation( components[ i ], ( Long )components[ i + 1 ], ( Long )components[ i + 1 ] );
                    components.RemoveAt(i);
                }
                else i++;
            }
            
            for (int i = 0; i < components.Count; )
            {
                if (components[i] == "*" || components[i] == "/")
                {
                    components[ i - 1 ] = singleCalculation( components[ i ], ( Long )components[ i - 1 ], ( Long )components[ i + 1 ] );
                    components.RemoveAt(i);
                    components.RemoveAt(i);
                }
                else i++;
            }

            for (int i = 0; i < components.Count;)
            {
                if (components[i] == "+" || components[i] == "-")
                {
                    components[ i - 1 ] = singleCalculation( components[ i ], ( Long )components[ i - 1 ], ( Long )components[ i + 1 ] );
                    components.RemoveAt(i);
                    components.RemoveAt(i);
                }
                else i++;
            }
            if (components.Count < 1)
                throw new InvalidLongException(INCORRECT_INPUT);
            
            return components[0];
        }
    }

}
