﻿#define UNIT_TEST

using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using HakamiLong;

namespace UnitTestHakamiLongNum
{
    [TestClass]
    public class TestHakamiLong
    {
        [TestMethod]
        public void HakamiLongNum_Parsed_Correctly()
        {

            Long myNum = (Long) "212345678901234567890";

            int[] digits = {7890, 3456, 9012, 5678, 1234, 2};
            UnsignedLong res = new UnsignedLong(digits, 6);
            Long result = new Long(res);

            Assert.AreEqual(result, myNum);
        }

        [TestMethod]
        public void HakamiLongNum_Equality() {
            Long myNum1 = ( Long ) "212345678901234567890";
            Long myNum2 = ( Long ) "212345678901234567890";

            if( myNum1 != myNum2 )
                Assert.Fail("Longs with same value. == returned false.");
            //if( myNum1.Equals( myNum2 ) )
            //    Assert.Fail("Different linked longs. Equals must return false.");
        }

        [TestMethod]
        public void HakamiLongNum_Cloned_Correctly()
        {
            Long num1 = ( Long ) "1234567875432345678765432345678765434567";
            Long cloned = num1.Clone();

            Assert.AreEqual(num1, cloned);
        }

        [TestMethod]
        public void HakamiLongNum_ToSting()
        {
            String s = "123456789012345678000000000000000000";
            Long myNum = ( Long ) s;
            Assert.AreEqual(s, myNum.ToString());
        }

        [TestMethod]
        public void HakamiLongNum_From_Right_Negative_String()
        {
            Long num = ( Long ) "-1234567890";

            int[] digits = { 7890, 3456, 12 };
            UnsignedLong res = new UnsignedLong(digits, 3);
            Long result = new Long(res, false);

            Assert.AreEqual(result, num);
        }

        [TestMethod]
        public void HakamiLongNum_From_Right_String_With_Leading_Zeros()
        {
            Long num = ( Long ) "-000000000000000000000000";
            Long result = new Long( new UnsignedLong() );

            Assert.AreEqual(result, num);
        }

        [TestMethod]
        public void HakamiLongNum_From_Fake_Negative_String_by3()
        {
            try
            {
                Long num = ( Long ) "123-567-901-345-723-567";
            }
            catch 
            {
                return;
            }

            Assert.Fail("Wrong num assepted and parsed");
        }

        [TestMethod]
        public void HakamiLongNum_From_Fake_Negative_String_by3and4()
        {
            try
            {
                Long num = ( Long ) "123-4567-4901-4345-4723-4567";
            }
            catch 
            {
                return;
            }
            Assert.Fail("Wrong num assepted and parsed");
        }

        [TestMethod]
        public void HakamiLongNum_From_Fake_Negative_String_by4and3()
        {
            try
            {
                Long num = ( Long ) "1124-456-490-434-472-456";
                Assert.Fail("Wrong num assepted and parsed");
            }
            catch 
            {
                return;
            }
            Assert.Fail("Wrong num assepted and parsed");
        }

        [TestMethod]
        public void HakamiLongNum_FromInt()
        {
            int x = 123456789;
            Long myNum = 123456789;
            Assert.AreEqual(myNum, x);

            Long myLolNum = -0;
            Assert.IsTrue(myLolNum.IsPositive);
        }

        [TestMethod]
        public void HakamiLongNum_ToInt()
        {
            int x = 123456789;
            Long num = ( Long ) "123456789";
            Assert.AreEqual(x, (int)num);

            Long num2 = ( Long ) "1000000";
            Assert.AreEqual( ( int )num2, 1000000 );

            Long num3 = ( Long ) "915000";
            Assert.AreEqual( ( int )num3, 915000 );
        }
  
        [TestMethod]
        public void HakamiLongNum_Comparing_2_positives_with_different_length_()
        {
            Long num1 = 123456;
            Long num2 = 5678;
            Assert.IsTrue(num1 > num2);

            Long num3 = ( Long ) "1000000";
            Long num4 = ( Long ) "915000";

            Assert.IsTrue( num3 > num4 );
        }

        [TestMethod]
        public void HakamiLongNum_Comparing_2_positives_with_same_length_()
        {
            Long num1 = 1234;
            Long num2 = 5678;
            Assert.IsTrue(num1 < num2);
        }

        [TestMethod]
        public void HakamiLongNum_Comparing_2_same_positives_()
        {
            Long num1 = 5678;
            Long num2 = 5678;
            Assert.IsTrue(num1.Equals(num2));
            Assert.IsTrue(num2 == num1);
        }

        [TestMethod]
        public void HakamiLongNum_Comparing_positive_and_negative()
        {
            Long num1 = ( Long ) "-12345678901234567890";
            Long num2 = ( Long ) "123456";

            Assert.IsTrue(num1 < num2);
        }

        [TestMethod]
        public void HakamiLongNum_Comparing_positive_and_negative_with_same_value()
        {
            Long num1 = ( Long ) "-12345678901234567890";
            Long num2 = ( Long ) "12345678901234567890";
            Assert.IsTrue(num1 < num2);
        
        }

        [TestMethod]
        public void HakamiLongNum_Summ_Natural_Positives()
        {
            Long num1 =  (Long)"12345678923456783456734567787654376543";
            Long num2 = (Long)"5547552521821217521418517582888825414178111157";
            Long result = (Long)"5547552534166896444875301039623393201832487700";
            
            Assert.AreEqual(result, num1 + num2);
        }

        [TestMethod]
        public void HakamiLongNum_Summ_BigPositive_And_Negative()
        {
            Long num1 = (Long)"9845168965414689451897451689745184";
            Long num2 = (Long)"-984516789645167";
            Long result = (Long)"9845168965414689450912934900100017";
            Long summ = num1 + num2;

            Assert.AreEqual( result, summ );
        }

        [TestMethod]
        public void HakamiLongNum_Summ_LittlePositive_And_Negative()
        {
            Long num1 = (Long)"-98456319748516397845123479864513122698";
            Long num2 = (Long)"2151468978456148974566489745";
            Long result = (Long)"-98456319746364928866667330889946632953";
            Long summ = num1 + num2;

            Assert.AreEqual(result, summ );
        }

        [TestMethod]
        public void HakamiLongNum_Summ_Negatives()
        {
            Long num1 = (Long)"-9999999999999999999999999999999999999999999999999999";
            Long num2 = (Long)"-9999999999999999999999999999999999999999999999999999";
            Long result = (Long)"-19999999999999999999999999999999999999999999999999998";
            Long summ = num1 + num2;

            Assert.AreEqual(result, summ );
        }

        [TestMethod]
        public void HakamiLongNum_Dif_Naturals()
        {
            Long num1 = (Long)"9999999999999999999999999999999999999999999999999999";
                                                  Long num2 = (Long)"99999999999999";
          Long result = (Long)"9999999999999999999999999999999999999900000000000000";

          Long realResult = num1 - num2;
            Assert.AreEqual(result, realResult );

            Long num3 = 72;
            Long num4 = 36;

            Assert.AreEqual(36, num3 - num4);
        }

        [TestMethod]
        public void HakamiLongNum_Dif_BigPositive_From_LittlePositive()
        {
            Long num1 = (Long)"999";
            Long num2 = (Long)"90000000000000000000000000000000000000000000000000000";
            Long result = (Long)"-89999999999999999999999999999999999999999999999999001";
            Long dif = num1 - num2;

            Assert.AreEqual(result, dif);
        }

        [TestMethod]
        public void HakamiLongNum_Dif_Negative_From_Positive()
        {
            Long num1 = (Long)"99999999999999999999999999999999999999999999";
            Long num2 = (Long)"-9999999999999999999999999999999999";
            Long result = (Long)"100000000009999999999999999999999999999999998";
            Long dif = num1 - num2;

            Assert.AreEqual(result, dif);
        }

        [TestMethod]
        public void HakamiLongNum_Dif_Positive_From_Negative()
        {
            Long num1 = (Long)"-99999999999999999999999999999999999999999999";
            Long num2 = (Long)"9999999999999999999999999999999999";
            Long result = (Long)"-100000000009999999999999999999999999999999998";
            Long dif = num1 - num2;

            Assert.AreEqual(result, dif);
        }

        [TestMethod]
        public void HakamiLongNum_Dif_BigNegative_From_LittleNegative()
        {
            Long num1 = (Long)"-79856198465123485";
            Long num2 = (Long)"-79845613284561784567845679845617894561";
            Long result = (Long)"79845613284561784567765823647152771076";
            Long dif = num1 - num2;

            Assert.AreEqual(result, dif);
        }

        [TestMethod]
        public void HakamiLongNum_Dif_LittleNegative_From_BigNegative()
        {
            Long num1 = (Long)"-79845613284561784567845679845617894561";
            Long num2 = (Long)"-79856198465123485";
            Long result = (Long)"-79845613284561784567765823647152771076";
            Long dif = num1 - num2;

            Assert.AreEqual(result, dif);
        }

        [TestMethod]
        public void HakamiLong_Multiply()
        {
            Long num1 = (Long)"79845613284561784567845679845617894561";
            Long num2 = (Long)"-79856198465123485";
            Long result = (Long)"-6376167141021466124560902979211873572394396174574865085";
            Long mult = num1*num2;

            Assert.AreEqual(result, mult);
        }

        [TestMethod]
        public void HakamiLong_SimpleDivide() {
            Long num1 = (Long)"66";
            Long num2 = (Long)"2";
            Long result = (Long)"33";
            Long dif = num1 / num2;

            Assert.AreEqual( result, dif );
        }

        [TestMethod]
        public void HakamiLong_Divide()
        {
            Long num1 = (Long)"79845613284";//561784567845679845617894561";
            Long num2 = (Long)"-1000000000";//000000000000000000000000000";//"-79856198465123485";
            Long result = (Long)"-79";//"-999867446976375623243";
            Long dif = num1/num2;

            Assert.AreEqual(result, dif);
        }

        [TestMethod]
        public void HakamiUnsignedLong_Divide()
        {
            UnsignedLong num1 = (UnsignedLong)"1000000";
            UnsignedLong num2 = ( UnsignedLong )"5000";
            UnsignedLong result = ( UnsignedLong )"200";
            UnsignedLong division = num1/num2;

            Assert.AreEqual(result, division);
        }

        [TestMethod]
        public void HakamiUnsignedLong_Div2()
        {
            UnsignedLong num = (UnsignedLong)"1000000000000000000000";
            UnsignedLong result =  (UnsignedLong)"500000000000000000000";
            UnsignedLong divided = num.div2();

            Assert.AreEqual(result, divided);
        }

        [TestMethod]
        public void HakamiLong_Sqrt()
        {
            Long num = 1000000;
            Long result = num.Sqrt();
            Long answer = 1000;

            Assert.AreEqual(answer, result);
        }

        [TestMethod]
        public void HakamiLong_Sqrt_BiggerNum() {
            Long num = 17;
            Long result = num.Sqrt();
            Long answer = 4;

            Assert.AreEqual( answer, result );
        }

        [TestMethod]
        public void HakamiLong_Sqrt_ReallyBigger() {
            Long num = 15;
            Long result = num.Sqrt();
            Long answer = 3;

            Assert.AreEqual( answer, result );
        }

        [TestMethod]
        public void HakamiLong_Log2()
        {
            Long num = 128;
            Long result = num.Log2();
            Long answer = 7;

            Assert.AreEqual(answer, result);
        }

        [TestMethod]
        public void HakamiLong_Log2_BiggerNum() {
            Long num = 17;
            Long result = num.Log2();
            Long answer = 4;

            Assert.AreEqual( answer, result );
        }

        [TestMethod]
        public void HakamiLong_Log2_Smaller() {
            Long num = 127;
            Long result = num.Log2();
            Long answer = 6;

            Assert.AreEqual( answer, result );
        }
    }
}
